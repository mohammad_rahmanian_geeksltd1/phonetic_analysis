﻿namespace Phonetic_Analysis
{
    using Olive.Csv;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;

    class Program
    {
        static async Task Main(string[] args)
        {

            // Read data from phonetics.csv file
            DataTable phonetics = await CsvReader.ReadAsync(new FileInfo("Phonetics.csv"), isFirstRowHeaders: true, minimumFieldCount: 2);

            // Read data from phonemes.csv file
            DataTable phonemes = await CsvReader.ReadAsync(new FileInfo("phonemes.csv"), isFirstRowHeaders: true, minimumFieldCount: 2);

            // Convert phonetics datatable to enumerable list
            var phoneticsQuery = from p in phonetics.AsEnumerable() select new { Word = p.Field<string>("Word"), StandardPronunciation = p.Field<string>("StandardPronunciation") };

            // Convert phonemes datatable to enumerable list
            var phonemesQuery = from p in phonemes.AsEnumerable() select new { IPASymbol = p.Field<string>("IPASymbol"), Graphemes = p.Field<string>("Graphemes") };

            float totalPhoneticsCount = phoneticsQuery.Count();

            //Name value dictionary for data be exported to csv
            Dictionary<string, string> analysisExport = new Dictionary<string, string>();




            //loop phonemes to analysis each phoneme from phonetics
            foreach (var ipa in phonemesQuery)
            {

                // split each graphem by predifined character
                string[] graphemeList = ipa.Graphemes.Split('|');

                Dictionary<string, string> graphmeAnalysis = new Dictionary<string, string>();

                // find word with each ipa in phonetics
                var ipaInPhoneticList = phoneticsQuery.Where(x => x.StandardPronunciation.ToLower().Contains(ipa.IPASymbol)).ToList();

                // variable to sum each graphem for ipa
                float totalIPACount = ipaInPhoneticList.Count();

                //loop for each graphem
                foreach (var graphem in graphemeList)
                {
                    //Query phonetics for each graphem and ipa
                    float graphemCount = ipaInPhoneticList.Count(x => x.Word.ToLower().Contains(graphem) && (x.Word.IndexOf(graphem) >= x.StandardPronunciation.IndexOf(ipa.IPASymbol) - 1 || x.Word.IndexOf(graphem) <= x.StandardPronunciation.IndexOf(ipa.IPASymbol) + 1));

                    // calculate percentages
                    float graphemPercentage = (graphemCount / totalIPACount) * 100;

                    graphmeAnalysis.Add(graphem, graphemPercentage.ToString("F2") + "%");

                }
                //calculate total percentage for total ipa count
                var totalIPAPercentage = (totalIPACount / totalPhoneticsCount) * 100;

                // add to graphem analysis dictionary
                graphmeAnalysis.Add("Total_" + ipa.IPASymbol, totalIPAPercentage.ToString("F2") + "%");

                // Add each ipa anlysis to final analysus
                var strJoinDictionary = string.Join(" | ", graphmeAnalysis.Select(x => x.Key + "=" + x.Value).ToArray());

                analysisExport.Add(ipa.IPASymbol, strJoinDictionary);

            }

            //write the string value to file
            analysisExport.ToCsv().Save(new FileInfo("AnalysisResult.csv"));
            //write the string value to console
            Console.WriteLine(analysisExport.ToCsv());


            Console.ReadKey();
        }
    }
}
